import TranzzoKeys._

val baseSettings: BaseSettings = BaseSettings(
  version = Version.mk("0.0.1", persistTo = "version"),
  scalaVer = ScalaVer("2.12.11"),
  organization = Tranzzo("encrypt-tool")
)

lazy val `encrypt-utility` = create single module named "encrypt-utility" withConfig(
  compileLibs = Seq(pureсonfig, `doobie-core`, `doobie-postgres`, `doobie-hikari`, `scala-logging`),
  testLibs = Seq()
) from baseSettings