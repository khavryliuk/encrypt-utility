create table p2p_credit_request
(
    id serial not null
        constraint p2p_credit_request_pkey
            primary key,
    uuid uuid not null,
    api_key uuid not null,
    pos_id uuid not null,
    created_ip varchar(45) not null,
    created_time timestamp not null,
    processed_time timestamp,
    pay_request_id uuid not null
        constraint pay_request_uuid_fk
            references pay_request (uuid),
    description varchar(2048),
    merchant_descriptor varchar(1024),
    card_number varchar(64),
    card_token varchar(8192),
    order_id varchar(256),
    order_amount numeric not null,
    order_currency varchar(256),
    credit_amount numeric,
    server_url varchar(1024),
    server_ack boolean,
    pos_response varchar(10560),
    status varchar(1024),
    company_id uuid,
    ik_server_url varchar(2048),
    kb_code varchar(64),
    kb_description varchar(1024),
    transaction_id uuid,
    registry_ref_no varchar(512),
    link varchar(1024),
    payment_id uuid
);