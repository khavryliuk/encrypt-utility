import sbt._

trait Versions {
  val `doobie-version`        = "0.9.0"
  val `scala-testkit-version` = "0.0.3"
  val `elastic4s-version`     = "6.7.3"
}

trait Dependencies extends Versions {

  val `doobie-core`           = "org.tpolecat"               %% "doobie-core"                       % `doobie-version`
  val `doobie-postgres`       = "org.tpolecat"               %% "doobie-postgres"                   % `doobie-version`
  val `doobie-hikari`         = "org.tpolecat"               %% "doobie-hikari"                     % `doobie-version`
  val `pureсonfig`            = "com.github.pureconfig"      %% "pureconfig"                        % "0.12.1"
  val `scala-logging`         = "com.typesafe.scala-logging" %% "scala-logging"                     % "3.9.2"

}

object TranzzoKeys extends Dependencies
