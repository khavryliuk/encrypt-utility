package com.tranzzo.encrypt.core

import cats.effect.{ContextShift, IO}
import com.tranzzo.encrypt.core.config.DBDataSource.DBConfig
import com.tranzzo.encrypt.core.config.{CryptoConfig, DBDataSource}
import com.tranzzo.encrypt.core.crypto.aes.{AesCardNumberCrypto, PBECipherComponents, PBECipherProvider}
import com.tranzzo.encrypt.core.repository.P2PCreditRepository
import doobie.util.transactor.Transactor

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

object Launcher extends App {
  implicit val ec: ExecutionContext = ExecutionContext.global
  implicit val ioContextShift: ContextShift[IO] = IO.contextShift(ec)

  val cryptoConfigPath = "./crypto.conf"
  val dbConfigPath = "./db.conf"

  val cryptoConfig: CryptoConfig = CryptoConfig.file(cryptoConfigPath)
  val DBdataSource: DBConfig = DBDataSource.file(dbConfigPath).dbConfig

  val pBECipherComponents = PBECipherComponents(cryptoConfig.cpayService)
  val pBECipherProvider = new PBECipherProvider(pBECipherComponents)
  val aesCardNumberCrypto = new AesCardNumberCrypto(pBECipherProvider)

  def makeTransactor(dataSource: DBConfig): Transactor[IO] =
    Transactor.fromDriverManager[IO](
      dataSource.driver,
      dataSource.url,
      dataSource.user,
      dataSource.password
    )

  val repo = new P2PCreditRepository(makeTransactor(DBdataSource), aesCardNumberCrypto)

  println("encrypt started")

  Await.result(repo.evalMigration(Some(300)).unsafeToFuture(), Duration.Inf)

  println("ready")

}
