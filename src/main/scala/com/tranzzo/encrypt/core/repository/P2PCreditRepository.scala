package com.tranzzo.encrypt.core.repository

import cats.effect.{Async, IO}
import cats.implicits._
import com.tranzzo.encrypt.core.crypto.aes.AesCardNumberCrypto
import com.tranzzo.encrypt.core.models.{DbMigrationReport, P2PCreditRequest}
import com.tranzzo.encrypt.core.repository.InsertionResult.MultipleInsertError
import com.typesafe.scalalogging.StrictLogging
import doobie.Update
import doobie.free.connection.ConnectionIO
import doobie.util.Read
import doobie.util.query.DefaultChunkSize
import doobie.util.transactor.Transactor
import doobie.implicits._

class P2PCreditRepository(val xa: Transactor[IO], crypto: AesCardNumberCrypto) extends StrictLogging with P2PCreditQueries {

  protected def preAction(rows: List[P2PCreditRequest]): IO[List[P2PCreditRequest]] =
    IO.delay(logger.info(s"Updating rows ${rows.length}")).map(_ => rows)

  protected def postAction(rows: List[P2PCreditRequest]): IO[Unit] =
    IO.delay(logger.info(s"Rows has been updated"))

  protected def onErrorAction(row: P2PCreditRequest, ex: Throwable): IO[InsertionResult] =
    IO.delay(logger.error(s"An error occurred during rows update - ${ex.getMessage}"))
      .map(_ => MultipleInsertError(1))

  def evalMigration(batchSize: Option[Int]): IO[DbMigrationReport] =
    selectToEncrypt()
      .query[P2PCreditRequest]
      .streamWithChunkSize(batchSize.getOrElse(DefaultChunkSize))
      .chunkN(batchSize.getOrElse(DefaultChunkSize))
      .evalMap(rows => Async[ConnectionIO].liftIO(evalRows(rows.toList)))
      .fold[DbMigrationReport](DbMigrationReport.empty)(foldMigrationReport)
      .compile
      .toList
      .map(_.headOption.getOrElse(DbMigrationReport.empty))
      .transact(xa)

  protected def evalRows(rows: List[P2PCreditRequest]): IO[List[InsertionResult]] =
    for {
      mutatedRows <- preAction(rows)
      result      <- mutatedRows.map(req => updateAction(encrypt(req), onErrorAction)).sequence
      _           <- postAction(rows)
    } yield result

  protected def updateAction(req: P2PCreditRequest, onError: (P2PCreditRequest, Throwable) => IO[InsertionResult]): IO[InsertionResult] =
    update(req)
      .run
      .transact(xa)
      .map(res => InsertionResult(res))
      .handleErrorWith { ex => println(ex.toString)
        IO.pure(InsertionResult.InsertError)}

  protected def foldMigrationReport: (DbMigrationReport, List[InsertionResult]) => DbMigrationReport = {
    case (report, insertionResult) if insertionResult.nonEmpty => report.mutate(insertionResult.head)
    case _                                                     => DbMigrationReport.empty
  }

  private def encrypt(req: P2PCreditRequest): P2PCreditRequest = req.copy(
    ccNumber = crypto.encrypt(req.ccNumber)
  )
}
