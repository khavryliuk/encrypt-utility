package com.tranzzo.encrypt.core.repository

sealed trait InsertionResult

object InsertionResult {

  case class InsertedMultipleRows(count: Int) extends InsertionResult
  case object InsertedOneRow extends InsertionResult
  case object InsertedNoRow  extends InsertionResult
  case object InsertError    extends InsertionResult
  case class MultipleInsertError(count: Int) extends InsertionResult

  def apply(int: Int): InsertionResult = int match {
    case value if value > 1 => InsertedMultipleRows(value)
    case 1                  => InsertedOneRow
    case 0                  => InsertedNoRow
    case _                  => InsertError
  }

}
