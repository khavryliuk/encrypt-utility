package com.tranzzo.encrypt.core.repository

import com.tranzzo.encrypt.core.models.P2PCreditRequest
import doobie.implicits._
import doobie.util.fragment.Fragment
import doobie.util.update.Update0

trait P2PCreditQueries extends DoobieMeta {
  def selectToEncrypt(): Fragment = {
    sql"""SELECT uuid, card_number FROM p2p_credit_request WHERE length(card_number) <= 20""".stripMargin
  }

  def update(p2PCreditRequest: P2PCreditRequest): Update0 = {
    import p2PCreditRequest._
    sql"""UPDATE p2p_credit_request SET card_number=$ccNumber
           WHERE uuid = CAST($uuid AS uuid)
         """.update
  }

}
