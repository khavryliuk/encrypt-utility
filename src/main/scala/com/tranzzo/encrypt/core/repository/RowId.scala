package com.tranzzo.encrypt.core.repository

trait RowId[T] {
  def id(t: T): String
}

object RowId {
  def apply[T](implicit rowId: RowId[T]): RowId[T] = rowId
}
