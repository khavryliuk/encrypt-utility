package com.tranzzo.encrypt.core.repository

import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.UUID

import doobie.util.meta.{Meta, MetaConstructors, MetaInstances, SqlMetaInstances}

trait DoobieMeta extends MetaConstructors with SqlMetaInstances with MetaInstances {

  implicit val localDateTimeMeta: Meta[LocalDateTime] =
    TimestampMeta.timap[LocalDateTime](_.toLocalDateTime)(Timestamp.valueOf)

  implicit val uuidMeta: Meta[UUID] =
    StringMeta.timap[UUID](UUID.fromString)(_.toString)

}
