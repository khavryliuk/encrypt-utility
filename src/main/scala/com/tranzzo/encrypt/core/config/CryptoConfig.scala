package com.tranzzo.encrypt.core.config

import com.tranzzo.encrypt.core.config.CryptoConfig.CpayCryptoConfig
import pureconfig.ConfigSource
import pureconfig.generic.auto._

case class CryptoConfig(cpayService: CpayCryptoConfig)

object CryptoConfig {

  def load(): CryptoConfig = ConfigSource.default.loadOrThrow[CryptoConfig]

  def file(path: String): CryptoConfig = ConfigSource.file(path).loadOrThrow[CryptoConfig]

  def resource(resource: String): CryptoConfig = ConfigSource.resources(resource).loadOrThrow[CryptoConfig]

  case class CpayCryptoConfig(
                                    salt:           String,
                                    key:            String,
                                    password:       String,
                                    keyLength:      Int,
                                    iterationCount: Int)
}
