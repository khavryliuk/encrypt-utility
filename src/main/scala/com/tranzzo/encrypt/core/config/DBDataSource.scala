package com.tranzzo.encrypt.core.config

import com.tranzzo.encrypt.core.config.DBDataSource.DBConfig
import pureconfig.ConfigSource
import pureconfig.generic.auto._
import scala.concurrent.duration.FiniteDuration

case class DBDataSource(dbConfig: DBConfig)

object DBDataSource {

  def load(): DBDataSource = ConfigSource.default.loadOrThrow[DBDataSource]

  def file(path: String): DBDataSource = ConfigSource.file(path).loadOrThrow[DBDataSource]

  def resource(resource: String): DBDataSource = ConfigSource.resources(resource).loadOrThrow[DBDataSource]

  case class DBConfig(
                       driver:            String,
                       url:               String,
                       user:              String,
                       password:          String,
                       connectionTimeout: FiniteDuration,
                       validationTimeout: FiniteDuration,
                       maxPoolSize:       Int)
}
