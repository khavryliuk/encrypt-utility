package com.tranzzo.encrypt.core.models

import java.util.UUID

case class P2PCreditRequest(uuid: UUID, ccNumber: String)
