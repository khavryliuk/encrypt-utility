package com.tranzzo.encrypt.core.models

import com.tranzzo.encrypt.core.repository.InsertionResult
import com.tranzzo.encrypt.core.repository.InsertionResult.{InsertError, InsertedMultipleRows, InsertedNoRow, InsertedOneRow, MultipleInsertError}

case class DbMigrationReport(totalRows: Int, errorRows: Int, insertedRows: Int, notInsertedRows: Int) {

  def mutate(insertionResult: InsertionResult): DbMigrationReport = insertionResult match {
    case InsertedOneRow =>
      copy(totalRows = totalRows + 1, insertedRows = insertedRows + 1)
    case InsertedMultipleRows(count) =>
      copy(totalRows = totalRows + count, insertedRows = insertedRows + count)
    case InsertedNoRow =>
      copy(totalRows = totalRows + 1, notInsertedRows = notInsertedRows + 1)
    case InsertError =>
      copy(totalRows = totalRows + 1, errorRows = errorRows + 1)
    case MultipleInsertError(count) =>
      copy(totalRows = totalRows + count, errorRows = errorRows + count)
  }

  def isSuccess: Boolean = totalRows == insertedRows

  override def toString: String =
    s"(totalRows = $totalRows, errorRows = $errorRows, insertedRows = $insertedRows, notInsertedRows = $notInsertedRows)"

}

object DbMigrationReport {
  def empty: DbMigrationReport = DbMigrationReport(0, 0, 0, 0)
}
