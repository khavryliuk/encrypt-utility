package com.tranzzo.encrypt.core.crypto

/**
 * Abstraction for encrypting credit card numbers
 */
trait CardNumberCrypto {

  /**
   * Facade for encryption.
   *
   * @param number card number to encrypt
   * @return ready to be persisted criptogram
   */
  def encrypt(number: String): String

  /**
   * Facade for decryption.
   *
   * @param cryptogram encrypted card number
   * @return decrypted plaintext
   */
  def decrypt(cryptogram: String): String

}
