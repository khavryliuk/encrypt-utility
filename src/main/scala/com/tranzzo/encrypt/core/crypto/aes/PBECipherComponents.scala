/*
 * Copyright (c) TRANZZO LTD - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.tranzzo.encrypt.core.crypto.aes

import java.nio.charset.StandardCharsets
import java.security.spec.KeySpec
import java.util.Base64

import com.tranzzo.encrypt.core.config.CryptoConfig.CpayCryptoConfig
import javax.crypto.spec.PBEKeySpec

/**
  * Holder for all components required for [[PBEKeySpec]] initialization.
  *
  * @param key        encryption key
  * @param password   password used during key generation
  * @param salt       salt used during key generation
  * @param iterations number of iteration used during [[PBEKeySpec]] generation
  */
case class PBECipherComponents(
  key:        Array[Byte],
  keyLength:  Int,
  password:   Array[Char],
  salt:       String,
  iterations: Int) {

  /** Initialize [[PBEKeySpec]] from components */
  def pbe(): KeySpec = new PBEKeySpec(password, salt.getBytes(StandardCharsets.UTF_8), iterations, keyLength)

  override def toString: String = "PBECipherComponents(***)"

}

object PBECipherComponents {

  def apply(cryproKeys: CpayCryptoConfig): PBECipherComponents = {
    import cryproKeys._
    PBECipherComponents(Base64.getDecoder.decode(key), keyLength, password.toCharArray, salt, iterationCount)
  }

}
