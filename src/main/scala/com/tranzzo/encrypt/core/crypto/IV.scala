/*
 * Copyright (c) TRANZZO LTD - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.tranzzo.encrypt.core.crypto

import java.security.SecureRandom

import javax.crypto.spec.IvParameterSpec

/**
  * Immutable type representing [[IvParameterSpec]] input bytes.
  *
  * @see [[IvParameterSpec]]
  */
case class IV private (bytes: Array[Byte]) {

  /**
    * Creates [[IvParameterSpec]] with a copy of [[bytes]] so that
    * mutations on original bytes array won't affect resulting spec.
    */
  def toSpec: IvParameterSpec = new IvParameterSpec(bytes)

}

object IV {

  def mk(length: Int, via: SecureRandom): IV = {
    val bytes = Array.ofDim[Byte](length)
    via.nextBytes(bytes)
    IV(bytes)
  }

}
