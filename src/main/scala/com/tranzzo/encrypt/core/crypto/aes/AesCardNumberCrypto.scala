/*
 * Copyright (c) TRANZZO LTD - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.tranzzo.encrypt.core.crypto.aes

import java.nio.charset.StandardCharsets
import java.nio.charset.StandardCharsets.UTF_8
import java.security.SecureRandom
import java.util.Base64

import com.tranzzo.encrypt.core.crypto.{CardNumberCrypto, CipherProvider, IV}

/**
  * Abstraction used for [de/en]crypting credit card number (but not only).
  */
class AesCardNumberCrypto(val cipherProvider: CipherProvider) extends CardNumberCrypto {

  import AesCardNumberCrypto._

  private val encoder = Base64.getEncoder
  private val decoder = Base64.getDecoder

  private val secureRandom = new SecureRandom()

  /**
    * Encrypt card number with optionally custom IV.
    *
    * @note iv parameter exists mostly more negative testing purposes
    * @param number card number (but not only) string to be encrypted
    * @return Base64-encoded cryptogram as a String with prepended IV
    */
  override def encrypt(number: String): String = {
    val iv = IV.mk(IvLength, via = secureRandom)

    val cipherText = cipherProvider.encryptor(iv).doFinal(number.getBytes(StandardCharsets.UTF_8))
    val cryptogram = iv.bytes ++ cipherText
    encoder.encodeToString(cryptogram)
  }

  /**
    * Decrypt card number with optionally custom IV.
    *
    * @note this implementation expects IV to be prepended to cipthertext.
    * @param cryptogram Base64-encoded cryptogram as a String with prepended IV
    * @return decrypted card number
    */
  override def decrypt(cryptogram: String): String = {
    val (iv, cipherText) = parseEncrypted(cryptogram)
    new String(cipherProvider.decryptor(iv).doFinal(cipherText), UTF_8)
  }

  private def parseEncrypted(cryptogram: String): (IV, Array[Byte]) = {
    val (iv, cipherText) = decoder.decode(cryptogram.getBytes(StandardCharsets.UTF_8)).splitAt(IvLength)
    (IV(iv), cipherText)
  }

}

object AesCardNumberCrypto {

  private val IvLength = 16

}
