package com.tranzzo.encrypt.core.crypto

import javax.crypto.Cipher

/**
 * Facade for providing preconfigured [[Cipher]] instances.
 */
trait CipherProvider {

  /**
   * Provides preconfigured [[Cipher]] instance for encrypting.
   *
   * @note [[Cipher]] is not thread safe though should be used by at most one thread at once
   * @param iv Initialization Vector
   */
  def encryptor(iv: IV): Cipher

  /**
   * Provides preconfigured [[Cipher]] instance for decrypting.
   *
   * @note [[Cipher]] is not thread safe though should be used by at most one thread at once
   * @param iv Initialization Vector
   */
  def decryptor(iv: IV): Cipher

}
