/*
 * Copyright (c) TRANZZO LTD - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.tranzzo.encrypt.core.crypto.aes

import com.tranzzo.encrypt.core.crypto.{CipherProvider, IV}
import javax.crypto.Cipher.{DECRYPT_MODE, ENCRYPT_MODE}
import javax.crypto.spec.SecretKeySpec
import javax.crypto.{Cipher, SecretKeyFactory}

/**
  * Provides preconfigured [[Cipher]] instances with {{{AES/CBC/PKCS5Padding}}} cipher spec
  * and {{{PBKDF2WithHmacSHA1}}} key storage.
  *
  * @param components holder for
  */
class PBECipherProvider(val components: PBECipherComponents) extends CipherProvider {

  import PBECipherProvider._

  private val cipherContainer = ThreadLocal.withInitial[Cipher](() => Cipher.getInstance(CIPHER_SPEC))

  /** @inheritdoc */
  override def encryptor(iv: IV): Cipher = initCipherIn(ENCRYPT_MODE, iv)

  /** @inheritdoc */
  override def decryptor(iv: IV): Cipher = initCipherIn(DECRYPT_MODE, iv)

  private val secretSpec: SecretKeySpec = {
    val factory = SecretKeyFactory.getInstance(SECRET_KEY_FACTORY_ALGORITHM)
    new SecretKeySpec(factory.generateSecret(components.pbe()).getEncoded, KEY_SPEC)
  }

  private def initCipherIn(mode: Int, iv: IV): Cipher = {
    val cipher = cipherContainer.get()
    cipher.init(mode, secretSpec, iv.toSpec)
    cipher
  }

}

object PBECipherProvider {

  private val SECRET_KEY_FACTORY_ALGORITHM = "PBKDF2WithHmacSHA1"
  private val CIPHER_SPEC                  = "AES/CBC/PKCS5Padding"

  private val KEY_SPEC = "AES"
}
